//Libs
const express = require("express");
const app = express();
const { connectDB } = require("./src/config/connectDB");
const configEngine = require("./src/config/viewEngine");
const routeWifiMKT = require("./src/routes/wifiMarketing")
const bodyParser = require("body-parser");

//connect Mongodb
connectDB();

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');//địa chỉ của angular đc phép truy cập vô nodejs này
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,X-CSRF-Token,raw,JSON');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

//config bodyParser for project
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//config router
routeWifiMKT(app) //wifimkt
//config view ejs for project
configEngine(app);
//config dotenv for project
require("dotenv").config({});

app.listen(process.env.PORT || 4000)
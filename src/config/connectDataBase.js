const mongoose = require('mongoose')

var SaleDB = mongoose.createConnection(process.env.MONGO_HOST + process.env.MONGO_PORT + process.env.MONGO_DB_NAME, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true 
})

var CouponDB = mongoose.createConnection(process.env.MONGO_HOST + process.env.MONGO_PORT + process.env.MONGO_DB_NAME_COUPON, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true 
})

module.exports = { SaleDB, CouponDB }


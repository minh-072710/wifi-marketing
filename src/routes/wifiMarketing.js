const express = require("express");
const router = express.Router();
const { Render_WifiScreen, Render_WifiScreen_Case_2 } = require("../controllers/wifi_marketing/wifi.controller");
let routerInit = (app) => {
    router.get("/wf-mkt-case-1", Render_WifiScreen)
    router.get("/wf-mkt-case-2", Render_WifiScreen_Case_2)
    app.use("/wifi", router)
}

module.exports = routerInit;
const Render_WifiScreen = (req, res) => {
    res.render("wifi_full_screen")
}
const Render_WifiScreen_Case_2 = (req, res) => {
    res.render("wifi_full_screen_case_2")
}
module.exports = {
    Render_WifiScreen,
    Render_WifiScreen_Case_2
}